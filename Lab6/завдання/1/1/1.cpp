﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Xml.Schema;
using System.Runtime.Remoting.Metadata.W3cXsd2001;

class Program
{

    static void SortByShella(int[] arr)
    {
        int step = arr.Length / 2;
        while (step > 0)
        {
            for (int i = 0; i < (arr.Length - step); i++)
            {
                int j = i;
                while (j >= 0 && arr[j] > arr[j + step])
                {
                    int c = arr[j];
                    arr[j] = arr[j + step];
                    arr[j + step] = c;
                    j--;
                }
            }
            step = step / 2;
        }
    }

    static void CountingSort(short[] arr)
    {
        int min = -10;
        int max = 100;
        int range = max - min + 1;
        int[] count = new int[range];
        short[] sortedArray = new short[arr.Length];


        for (int i = 0; i < arr.Length; i++)
        {
            count[arr[i] - min]++;
        }


        for (int i = 1; i < range; i++)
        {
            count[i] += count[i - 1];
        }


        for (int i = arr.Length - 1; i >= 0; i--)
        {
            sortedArray[count[arr[i] - min] - 1] = arr[i];
            count[arr[i] - min]--;
        }


        for (int i = 0; i < arr.Length; i++)
        {
            arr[i] = sortedArray[i];
        }
    }

    static void PrintArray(short[] arr)
    {
        for (int i = 0; i < arr.Length; i++)
        {
            Console.Write(arr[i] + " ");
        }
        Console.WriteLine();
    }



    static void Main()
    {

        Console.OutputEncoding = Encoding.UTF8;
        Console.InputEncoding = Encoding.UTF8;


        Stopwatch stopwatch = new Stopwatch();

        Random rn2 = new Random();





        int temp;
        int num = 0;

        bool set = true;

        stopwatch.Start();
        while (set)
        {

            Console.WriteLine("1 - Пірамідальне сортування \n" +
                "2- Сортування Шелла\n" +
                "3 - Сортування з вибором\n");
            num = int.Parse(Console.ReadLine());
            switch (num)
            {
            case 1:
                float[] numbers = new float[10];
                Random rn = new Random();
                Console.WriteLine("Основний масив");

                for (int i = 0; i < numbers.Length; i++)
                {
                    numbers[i] = (float)(rn.NextDouble() * (10 - (-100)) + (-100));
                    Console.Write($" {numbers[i]}");
                }

                for (int i = 0; i < numbers.Length; i++)
                {
                    for (int j = 0; j < numbers.Length - 1 - i; j++)
                    {
                        if (numbers[j] > numbers[j + 1])
                        {
                            float temp1 = numbers[j];
                            numbers[j] = numbers[j + 1];
                            numbers[j + 1] = temp1;
                        }
                    }
                }


                Console.WriteLine("\n");

                Console.WriteLine("Відсортований масив");
                for (int i = 0; i < numbers.Length; i++)
                {
                    Console.Write($" {numbers[i]}");
                }
                Console.WriteLine("\n");
                stopwatch.Stop();

                long elapsedTimeMs = stopwatch.ElapsedMilliseconds;
                Console.WriteLine("Час виконання програми: " + elapsedTimeMs + " мс");
                break;

            case 2:
                stopwatch.Start();
                Console.WriteLine("\nСортування методом Шелла");
                Random rnd2 = new Random();
                int[] arr2 = new int[10];
                for (int i = 0; i < arr2.Length; i++)
                {
                    arr2[i] = (int)(rnd2.NextDouble() * (10 - (0)) + (400));
                    Console.Write(arr2[i] + " ");
                }
                Console.WriteLine("\nВідсортований масив:");
                SortByShella(arr2);
                for (int i = 0; i < arr2.Length; i++)
                {
                    Console.Write(arr2[i] + " ");
                }
                Console.WriteLine("\n");
                stopwatch.Stop();
                long time = stopwatch.ElapsedMilliseconds;
                Console.WriteLine("Час викодання программи: " + time + "mc");
                break;
            case 3:
                stopwatch.Start();
                short[] numbers3 = { -2, 10, 5, 0, 8, -10, 100, 50, 25 };

                Console.WriteLine("Оригінальний масив:");
                PrintArray(numbers3);

                CountingSort(numbers3);

                Console.WriteLine("Відсортований масив:");
                PrintArray(numbers3);
                stopwatch.Stop();
                long time1 = stopwatch.ElapsedMilliseconds;
                Console.WriteLine("Час виконання програми: " + time1 + "mc");
                break;
            }
        }
    }
}
