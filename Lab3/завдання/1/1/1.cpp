﻿#include <stdio.h>
#include <math.h>
#include <stdint.h>

int main() {
    int n;
    double result;
    printf("f(n)=n\tf(n)=log(n)\tf(n)=n*log(n)\tf(n)=n^2\tf(n)=2^n\tf(n)=n!\n");

    for (n = 0; n <= 50; n++) {
        result = n;
        printf("%d\t", n, result);

        if (n > 0) {
            result = log(n);
            printf("%.3f\t\t", result);
        }
        else {
            printf("N/A\t\t");
        }

        result = n * log(n);
        printf("%.3f\t\t", result);

        result = pow(n, 2);
        printf("%.0f\t\t", result);

        result = pow(2, n);
        printf("%.0f\t\t", result);

        if (n == 0) {
            printf("1\n");
        }
        else {
            long long int fact = 1;
            int i;
            for (i = 1; i <= n; i++) {
                fact *= i;
            }
            printf("%.0lld\n", fact);
        }
    }
    return 0;
}

