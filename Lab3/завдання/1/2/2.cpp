﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <chrono>
#define GETTIME std::chrono::steady_clock::now
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>

int compare(const void* a, const void* b) {
    return (*(int*)b - *(int*)a);
}

int buildMaxNumber(int digits[], int n) {
    // Сортуємо цифри в порядку спадання
    qsort(digits, n, sizeof(int), compare);

    int maxNumber = 0;
    int power = 1;
    for (int i = 0; i < n; i++) {
        // Обчислюємо число, використовуючи порядок цифри
        maxNumber += digits[i] * power;
        power *= 10;
    }
    return maxNumber;
}

int main() {
    int digits[20];
    int n = 10; // Розмір масиву цифр
    srand(time(0)); // Ініціалізація генератора випадкових чисел
    for (int i = 0; i < n; i++) {
        digits[i] = rand() % 10; // Заповнення масиву випадковими цифрами
    }

    // Виводимо масив цифр
    printf("Digits: ");
    for (int i = 0; i < n; i++) {
        printf("%d ", digits[i]);
    }
    printf("\n");

    // Будуємо найбільше можливе число
    int maxNumber = buildMaxNumber(digits, n);

    // Виводимо результат
    printf("Max Number: %d\n", maxNumber);
    auto begin = GETTIME();
    getchar();
    auto end = GETTIME();
    auto elapsed_ns = CALCTIME(end - begin);
    printf("The time: %lld ns\n", elapsed_ns.count());
    return 0;
}
