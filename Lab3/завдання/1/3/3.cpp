﻿#include <stdio.h>
#include <chrono>
#define GETTIME std::chrono::steady_clock::now
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>

long long fibonacci(int n) {
    if (n == 0) {
        return 0;
    }
    else if (n == 1) {
        return 1;
    }
    else {
        long long a = 0, b = 1, c;
        for (int i = 2; i <= n; i++) {
            c = a + b;
            a = b;
            b = c;
        }
        return b;
    }
}

int main() {
    int n = 10;

    // Обчислюємо n-те число Фібоначчі та виводимо результат
    printf("Fibonacci(%d) = %lld\n", n, fibonacci(n));
    auto begin = GETTIME();
    getchar();
    auto end = GETTIME();
    auto elapsed_ns = CALCTIME(end - begin);
    printf("The time: %lld ns\n", elapsed_ns.count());
    return 0;
}
