﻿#include <stdio.h>
#include <math.h>
#include <chrono>

#define GETTIME std::chrono::steady_clock::now
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>

// Виправлено помилки в обчисленні значень функцій
int f1(int n) {
    return n;
}

int f2(int n) {
    return (n > 0) ? (int)(log(n) / log(2)) : 0;
}

int f3(int n) {
    return n * f2(n);
}

int f4(int n) {
    return n * n;
}

long f5(int n) {
    return (long)pow(2, n);
}

long f6(int n) {
    return (n <= 0) ? 1 : f6(n - 1) * n;
}

// Виправлено помилки в форматуванні таблиці та виклику функцій
void func_table() {
    printf(" n | f(n)=n | f(n)=log(n) | f(n)=nlog(n) | f(n)=n^2 | f(n)=2^n | f(n)=n!\n");
    printf("---|--------|-------------|--------------|----------|----------|-------\n");

    for (int n = 0; n <= 50; n++) {
        int y1 = f1(n);
        int y2 = f2(n);
        int y3 = f3(n);
        int y4 = f4(n);
        long y5 = f5(n);
        long y6 = f6(n);

        printf("%2d |%4d    |%7d        |%10d      |%5d     |%10ld|%7ld\n", n, y1, y2, y3, y4, y5, y6);
    }
}

int main() {
    auto begin = GETTIME();
    getchar();
    auto end = GETTIME();
    auto elapsed_ns = CALCTIME(end - begin);
    printf("The time: %lld ns\n", elapsed_ns.count());

    func_table();
    return 0;
}

