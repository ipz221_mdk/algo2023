﻿#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#define _CRT_SECURE_NO_WARNINGS

int main()
{
    union data8 {
        struct {
            unsigned char byte1 : 1;
            unsigned char byte2 : 1;
            unsigned char byte3 : 1;
            unsigned char byte4 : 1;
            unsigned char byte5 : 1;
            unsigned char byte6 : 1;
            unsigned char byte7 : 1;
            unsigned char byte8 : 1;
        }bytes;
        signed short number;
    };
    short number;
    scanf_s("%d", &number);
    union data8 num;

    num.number = number;
    printf("Sign: %d\n", num.bytes.byte8);
    printf("Value: %d %d %d %d %d %d %d\n\n", num.bytes.byte7, num.bytes.byte6, num.bytes.byte5, num.bytes.byte4, num.bytes.byte3, num.bytes.byte2, num.bytes.byte1);
    if (number < 0) {
        printf("Sign: -\n");
        printf("Value: %d\n\n", number * -1);
    }
    else
    {
        printf("Sign: +\n");
        printf("Value: %d\n\n", number);
    }
    return 0;
}
