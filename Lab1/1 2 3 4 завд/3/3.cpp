﻿#include <stdio.h>

int main() {
	const signed char a = 5, a1 = 127, b = 2, b1 = 3, c = -120, c1 = 34, d = -5, d1 = 56, i = 38; printf("5 + 127 = %hhi \n", a + a1);
	printf("2 - 3 = %hhi \n", b - b1);
	printf("-120 - 34 = %hhi \n", c - c1);
	printf("-5 = %hhu \n", (unsigned char)d);
	printf("56 & 38 = %hhi \n", d1 & i);
	printf("56 | 38 = %hhi \n", d1 | i);

	return 0;
}