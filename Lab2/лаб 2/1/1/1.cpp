﻿#include <iostream>
#include <Windows.h>
#include <stdint.h>
#include <math.h>
#define N 30000

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    unsigned int x0, a = 65539, c = 0, k = 30000;
    int arr[N], arr1[100];
    float x = 1;
    int m = pow(2, 32);

    printf("Введіть початкове значення x: ");
    scanf_s("%d", &x0);

    for (int i = 0; i < k; i++)
    {
        x0 = (x0 * a + c) % m;
        x = ((float)x0 / m * 200 * (-1));
        arr[i] = x;
        printf(" %5d", arr[i]);
    }

    printf("\n\nЧастота появи випадкових величин:\n");
    int f = 0, g = 0;
    float arr2[100];

    for (int h = 0; h < 100; h++)
    {
        for (int l = 0; l < k; l++)
        {
            if (arr[l] == h)
                f++;
        }
        printf("  %d | %d\n", h, f);
        arr1[h] = f;
        f = 0;
    }

    printf("\nСтатистична ймовірність появи випадкових величин:\n");

    for (int h = 0; h < 200; h++)
    {
        for (int l = 0; l < k; l++)
        {
            if (arr[l] == h)
                g++;
        }
        printf("  %d | %lf\n", h, (float)g / 30000);
        arr2[h] = (float)g / 30000;
        g = 0;
    }

    double  M = 0, D = 0, B = 0;

    for (int i = 0; i < 200; i++)
    {
        M += arr[i] * arr2[i];
    }

    printf("\nМатематичне сподівання появи випадкових величин: %lf\n", M);

    for (int i = 0; i < k; i++)
    {
        D += pow(arr[i] - M, 2) * arr[i];
    }

    printf("Дисперсія випадкових велечин: %lf\n", D);
    B = sqrt(D);
    printf("Середньо квадратичне відхілення випадкової велечини: %lf\n", B);

    return 0;
}