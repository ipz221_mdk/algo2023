﻿using System;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        Console.OutputEncoding = System.Text.Encoding.Unicode;
        Console.InputEncoding = System.Text.Encoding.Unicode;
        Console.WriteLine("Довідка:");
        Console.WriteLine("Щоб закінчити введи 0");
        Console.WriteLine("Введи вираз через пробіл");
        Console.WriteLine("Наприклад: 2 + 3 * sqrt 4");
        Console.WriteLine("'+' - додавання");
        Console.WriteLine("'-' - віднімання");
        Console.WriteLine("'*' - множення");
        Console.WriteLine("'/' - ділення");
        Console.WriteLine("'^' - Піднесення у степінь");
        Console.WriteLine("'sqrt' - квадратний корінь");

        while (true)
        {
            Console.Write("Введіть вираз: ");
            string input = Console.ReadLine();

            if (input == "0")
                break;

            try
            {
                double result = CalculateExpression(input);
                Console.WriteLine("Результат: " + result);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Помилка: " + ex.Message);
            }
        }
    }

    static double CalculateExpression(string input)
    {
        string[] tokens = input.Split(' ');
        Stack<string> stack = new Stack<string>();
        Queue<string> queue = new Queue<string>();

        foreach(string token in tokens)
        {
            if (IsNumber(token))
            {
                queue.Enqueue(token);
            }
            else if (IsOperator(token))
            {
                while (stack.Count > 0 && IsOperator(stack.Peek()) && OperatorPriority(token) <= OperatorPriority(stack.Peek()))
                {
                    queue.Enqueue(stack.Pop());
                }
                stack.Push(token);
            }
            else if (token == "sqrt")
            {
                if (queue.Count < 1)
                    throw new ArgumentException("Неправильний вираз. Не вистачає операндів");

                double op1 = double.Parse(queue.Dequeue());
                double result = ApplyOperator(op1, 0, token);
                queue.Enqueue(result.ToString());
            }
            else if (token == "(")
            {
                stack.Push(token);
            }
            else if (token == ")")
            {
                while (stack.Count > 0 && stack.Peek() != "(")
                {
                    queue.Enqueue(stack.Pop());
                }
                if (stack.Count == 0)
                    throw new ArgumentException("Неправильний вираз. Не вистачає дужки");

                stack.Pop();
            }
            else
            {
                throw new ArgumentException("Неправильний оператор або число");
            }
        }

        while (stack.Count > 0)
        {
            if (stack.Peek() == "(")
                throw new ArgumentException("Неправильний вираз. Не вистачає дужки");

            queue.Enqueue(stack.Pop());
        }

        Stack<double> resultStack = new Stack<double>();
        foreach(string token in queue)
        {
            if (IsNumber(token))
            {
                resultStack.Push(double.Parse(token));
            }
            else if (token == "sqrt")
            {
                if (resultStack.Count < 1)
                    throw new ArgumentException("Неправильний вираз. Не вистачає операндів");

                double op1 = resultStack.Pop();
                double result = ApplyOperator(op1, 0, token);
                resultStack.Push(result);
            }
            else if (IsOperator(token))
            {
                if (resultStack.Count < 2)
                    throw new ArgumentException("Неправильний вираз. Не вистачає операндів");

                double op2 = resultStack.Pop();
                double op1 = resultStack.Pop();
                double result = ApplyOperator(op1, op2, token);
                resultStack.Push(result);
            }
        }

        if (resultStack.Count == 0)
            throw new ArgumentException("Неправильний вираз");

        return resultStack.Pop();
    }

    static bool IsNumber(string token)
    {
        return double.TryParse(token, out _);
    }

    static bool IsOperator(string token)
    {
        return token == "+" || token == "-" || token == "*" || token == "/" || token == "^" || token == "sqrt";
    }

    static int OperatorPriority(string op)
    {
        switch (op)
        {
        case "+":
        case "-":
            return 1;
        case "*":
        case "/":
            return 2;
        case "^":
            return 3;
        case "sqrt":
            return 4;
        default:
            throw new ArgumentException("Неправильний оператор");
        }
    }

    static double ApplyOperator(double op1, double op2, string op)
    {
        switch (op)
        {
        case "+":
            return op1 + op2;
        case "-":
            return op1 - op2;
        case "*":
            return op1 * op2;
        case "/":
            if (op2 == 0)
                throw new DivideByZeroException();

            return op1 / op2;
        case "^":
            return Math.Pow(op1, op2);
        case "sqrt":
            return Math.Sqrt(op1);
        default:
            throw new ArgumentException("Неправильний оператор");
        }
    }
}

