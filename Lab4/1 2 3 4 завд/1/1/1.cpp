﻿#include <stdio.h>
#include <stdlib.h>

// Структура для представлення вузла списку
struct Node {
    int data;
    struct Node* prev;
    struct Node* next;
};

// Двозв'язний список
struct DoublyLinkedList {
    struct Node* head;
    struct Node* tail;
};

// Ініціалізація списку
void initialize(struct DoublyLinkedList* list) {
    list->head = NULL;
    list->tail = NULL;
}

// Додавання елемента в кінець списку
void add(struct DoublyLinkedList* list, int data) {
    struct Node* new_node = (struct Node*)malloc(sizeof(struct Node));
    new_node->data = data;
    new_node->prev = list->tail;
    new_node->next = NULL;

    if (list->tail != NULL) {
        list->tail->next = new_node;
    }
    else {
        list->head = new_node;
    }

    list->tail = new_node;
}

// Вставка елемента на задану позицію
void insert(struct DoublyLinkedList* list, int data, int index) {
    if (index < 0) {
        printf("Неприпустимий індекс\n");
        return;
    }

    struct Node* new_node = (struct Node*)malloc(sizeof(struct Node));
    new_node->data = data;
    new_node->prev = NULL;
    new_node->next = NULL;

    if (index == 0) {
        if (list->head != NULL) {
            new_node->next = list->head;
            list->head->prev = new_node;
        }
        else {
            list->tail = new_node;
        }

        list->head = new_node;
    }
    else {
        struct Node* current = list->head;
        int i = 0;

        while (current != NULL && i < index - 1) {
            current = current->next;
            i++;
        }

        if (current == NULL) {
            printf("Неприпустимий індекс\n");
            free(new_node);
            return;
        }

        new_node->next = current->next;
        new_node->prev = current;

        if (current->next != NULL) {
            current->next->prev = new_node;
        }
        else {
            list->tail = new_node;
        }

        current->next = new_node;
    }
}

// Видалення елемента за заданим індексом
void remove_by_index(struct DoublyLinkedList* list, int index) {
    if (index < 0) {
        printf("Неприпустимий індекс\n");
        return;
    }

    if (list->head == NULL) {
        printf("Список порожній\n");
        return;
    }

    struct Node* current = list->head;
    int i = 0;

    while (current != NULL && i < index) {
        current = current->next;
        i++;
    }

    if (current == NULL) {
        printf("Неприпустимий індекс\n");
        return;
    }

    if (current->prev != NULL) {
        current->prev->next = current->next;
    }
    else {
        list->head = current->next;
    }

    if (current->next != NULL) {
        current->next->prev = current->prev;
    }
    else {
        list->tail = current->prev;
    }

    free(current);
}

// Видалення елемента за заданим значенням
void remove_by_value(struct DoublyLinkedList* list, int data) {
    struct Node* current = list->head;

    while (current != NULL) {
        if (current->data == data) {
            if (current->prev != NULL) {
                current->prev->next = current->next;
            }
            else {
                list->head = current->next;
            }

            if (current->next != NULL) {
                current->next->prev = current->prev;
            }
            else {
                list->tail = current->prev;
            }

            free(current);
            return;
        }

        current = current->next;
    }

    printf("Елемент не знайдено\n");
}

// Повертає розмір списку
int size(struct DoublyLinkedList* list) {
    struct Node* current = list->head;
    int count = 0;

    while (current != NULL) {
        count++;
        current = current->next;
    }

    return count;
}

// Знаходить і повертає індекс першого елемента з заданим значенням
int find(struct DoublyLinkedList* list, int data) {
    struct Node* current = list->head;
    int index = 0;

    while (current != NULL) {
        if (current->data == data) {
            return index;
        }

        current = current->next;
        index++;
    }

    return -1;
}

// Виведення елементів списку
void print(struct DoublyLinkedList* list) {
    struct Node* current = list->head;

    while (current != NULL) {
        printf("%d ", current->data);
        current = current->next;
    }

    printf("\n");
}

// Очищення пам'яті, виділеної для списку
void clear(struct DoublyLinkedList* list) {
    struct Node* current = list->head;

    while (current != NULL) {
        struct Node* next = current->next;
        free(current);
        current = next;
    }

    list->head = NULL;
    list->tail = NULL;
}

// Функція main
int main() {
    struct DoublyLinkedList list;
    initialize(&list);

    add(&list, 10);
    add(&list, 20);
    add(&list, 30);

    print(&list);  // Виведення: 10 20 30

    insert(&list, 15, 1);
    insert(&list, 25, 3);

    print(&list);  // Виведення: 10 15 20 25 30

    remove_by_index(&list, 2);
    remove_by_value(&list, 15);

    print(&list);  // Виведення: 10 20 25 30

    int index = find(&list, 25);
    if (index != -1) {
        printf("Елемент знайдено на позиції %d\n", index);  // Виведення: Елемент знайдено на позиції 2
    }
    else {
        printf("Елемент не знайдено\n");
    }

    printf("Розмір списку: %d\n", size(&list));  // Виведення: Розмір списку: 4

    clear(&list);

    print(&list);  // Виведення: (порожній список)

    return 0;
}
