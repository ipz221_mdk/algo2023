﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <Windows.h>

void insertionSort(int arr[], int n) {
    for (int i = 1; i < n; i++) {
        int key = arr[i];
        int j = i - 1;
        while (j >= 0 && arr[j] > key) {
            arr[j + 1] = arr[j];
            j--;
        }
        arr[j + 1] = key;
    }
}

void printArray(int arr[], int n) {
    for (int i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main() {
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    srand(time(0));
    int arr[5000];
    int num = 5000;
    for (int i = 0; i < num; i++) {
        arr[i] = rand() % 1000;
    }
    printf("Перед сортуванням:\n");
    printArray(arr, num);

    clock_t start = clock();
    insertionSort(arr, num);
    clock_t finish = clock();

    printf("\nПісля сортування:\n");
    printArray(arr, num);

    float elapsed_time = (float)(finish - start) / CLOCKS_PER_SEC;
    printf("Час сортування: %.4f сек\n", elapsed_time);

    return 0;
}

