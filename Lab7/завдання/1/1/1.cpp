﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

class Program
{
    private static readonly string[] city = new string[19]
    {
       "1 Київ",
       "2 Житомир",
       "3 Новоград-Волинський",
       "4 Рівне",
       "5 Луцьк",
       "6 Бердичів",
       "7 Вінниця",
       "8 Хмельницький",
       "9 Тернопіль",
       "10 Шепетівка",
       "11 Біла церква",
       "12 Умань",
       "13 Черкаси",
       "14 Кременчуг",
       "15 Полтава",
       "16 Харків",
       "17 Прилуки",
       "18 Суми",
       "19 Миргород"
    };
    private static readonly int[, ] mat = new int[, ]
    {
        {0,  135,    0,    0,    0,    0,    0,    0,    0,    0,   78,    0,    0,    0,    0,    0,  128,    0,    0 },
        {0,    0,   80,    0,    0,   38,    0,    0,    0,  115,    0,    0,    0,    0,    0,    0,    0,    0,    0 },
        {0,    0,    0,  100,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0 },
        {0,    0,    0,    0,   68,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0 },
        {0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0 },
        {0,    0,    0,    0,    0,    0,   73,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0 },
        {0,    0,    0,    0,    0,    0,    0,  110,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0 },
        {0,    0,    0,    0,    0,    0,    0,    0,  104,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0 },
        {0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0 },
        {0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0 },
        {0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,  115,  146,    0,  181,    0,    0,    0,    0 },
        {0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0 },
        {0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,  105,    0,    0,    0,    0,    0 },
        {0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0 },
        {0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,  130,    0,    0,    0 },
        {0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0 },
        {0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,  175,  109 },
        {0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0 },
        {0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,    0 },
    };
    private static void Bfs()
    {
        int[] k = new int[20];
        bool[] e = new bool[20];
        int cityIn = 0;
        int temp = 1;
        e[cityIn] = true;
        var queue = new Queue<int>();
        queue.Enqueue(cityIn);
        while (queue.Count != 0)
        {
            cityIn = queue.Dequeue();
            for (int i = 0; i < city.Length; i++)
            {
                if (mat[cityIn, i] != 0 && !e[i])
                {
                    e[i] = true;
                    queue.Enqueue(i);
                    k[i] = k[cityIn] + mat[cityIn, i];
                    Console.WriteLine($"{temp++}. Київ - {city[i]} ({k[i]}км)");
                }
            }

        }
    }
    static int path = 0;
    private static void Dfs(int cityI, string route, int cityForOutput)
    {

        if (cityI != 0)
        {
            route = $"{route} - {city[cityI]}";
            path += mat[cityForOutput, cityI];
            Console.WriteLine($" {route} ({path}км)");
        }
        else
        {
            route = $"{city[cityI]}";
        }
        for (int i = 0; i < city.Length; i++)
            if (mat[cityI, i] != 0)
            {
                Dfs(i, route, cityI);
                path -= mat[cityI, i];
            }
    }

    static void Main()
    {
        Console.InputEncoding = Encoding.Unicode;
        Console.OutputEncoding = Encoding.Unicode;
        int cityIndex = 0;
        string route = "";
        int temp = 0;
        bool temp1 = true;
        while (temp1) {

            Console.WriteLine("1 - Можливі маршрути\n" +
                "2 - Відставнь між Києва до міст\n" +
                "3 - Матриця суміжності\n" +
                "4 - Завергити роботу\n");
            temp = int.Parse(Console.ReadLine());
            switch (temp)
            {
            case 1:
                Console.Clear();
                Console.WriteLine("\nМожливі маршрути\n");
                Dfs(cityIndex, route, 0);
                break;
            case 2:
                Console.Clear();
                Console.WriteLine("\nВідстань від Києва до Міст\n");
                Bfs();
                Console.WriteLine("\n");
                break;
            case 3:
                Console.Clear();
                Console.WriteLine("\nМатриця суміжності\n");
                for (int i = 0; i < 19; i++)
                {
                    for (int j = 0; j < 19; j++)
                    {
                        Console.Write($"{mat[i, j],5},");
                    }
                    Console.WriteLine();
                }
                break;
            case 4:
                temp1 = false;
                break;
            }

        }
    }
}
