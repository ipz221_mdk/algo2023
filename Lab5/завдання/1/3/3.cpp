﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct Node {
    int data;
    struct Node* next;
    struct Node* prev;
} Node;

void insert(Node** head, int data) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    newNode->data = data;
    newNode->next = NULL;
    newNode->prev = NULL;
    if (*head == NULL) {
        *head = newNode;
        return;
    }
    Node* temp = *head;
    if (temp->next == NULL) {
        temp->next = newNode;
        newNode->prev = temp;
        return;
    }
    newNode->next = temp->next;
    newNode->prev = temp;
    temp->next->prev = newNode;
    temp->next = newNode;
}

void insertionSort(Node** head) {
    if (*head == NULL || (*head)->next == NULL) {
        return;
    }
    Node* sorted = NULL;
    Node* current = *head;
    while (current != NULL) {
        Node* next = current->next;
        if (sorted == NULL || sorted->data >= current->data) {
            current->next = sorted;
            if (sorted != NULL) {
                sorted->prev = current;
            }
            sorted = current;
        }
        else {
            Node* temp = sorted;
            while (temp->next != NULL && temp->next->data < current->data) {
                temp = temp->next;
            }
            current->next = temp->next;
            current->prev = temp;
            if (temp->next != NULL) {
                temp->next->prev = current;
            }
            temp->next = current;
        }
        current = next;
    }
    *head = sorted;
}

void printList(Node* head) {
    Node* temp = head;
    while (temp != NULL) {
        printf("%d ", temp->data);
        temp = temp->next;
    }
    printf("\n");
}

int main() {
    float dd;
    clock_t start, finish;
    srand(time(NULL));
    Node* head = NULL;
    int n = 10;
    int arr[10];
    for (int i = 0; i < n; i++) {
        arr[i] = rand() % 1000;
    }
    for (int i = 0; i < n; i++) {
        insert(&head, arr[i]);
    }
    printf("Before sorting:\n");
    printList(head);
    start = clock();
    insertionSort(&head);
    finish = clock();
    printf("After sorting:\n");
    printList(head);
    dd = ((float)(finish - start)) / CLOCKS_PER_SEC;
    printf("Time = %f\n", dd);
    return 0;
}

