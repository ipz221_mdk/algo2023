﻿#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void insertionSort(int arr[], int n) {
    for (int i = 1; i < n; i++) {
        int key = arr[i];
        int j = i - 1;
        while (j >= 0 && arr[j] > key) {
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = key;
    }
}

void printArray(int arr[], int n) {
    for (int i = 0; i < n; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main()
{
    srand(time(0));
    int n = 5000;
    int arr[5000];
    for (int i = 0; i < n; i++)
    {
        arr[i] = rand() % 1000;
    }
    printf("Перед сортуванням:\n");
    printArray(arr, n);
    insertionSort(arr, n);
    printf("Після сортування:\n");
    printArray(arr, n);
    return 0;
}
